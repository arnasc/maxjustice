#include "TitleState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
#include "MusicPlayer.hpp"



#include <SFML/Graphics/RenderWindow.hpp>

bool	introMusicPlayed;
TitleState::TitleState(StateStack& stack, Context context)
	: State(stack, context)
	,mText()
	,mShowText(true)
	,mTextEffectTime(sf::Time::Zero)
{
	mBackgroundSprite.setTexture(context.textures->get(Textures::TitleScreen));

	mText.setFont(context.fonts->get(Fonts::Main));
	mText.setString("Press any key to start");
	mText.setCharacterSize(50);
	mText.setStyle(sf::Text::Bold);
	mText.setFillColor(sf::Color::Red);
	mText.setOutlineColor(sf::Color::Black);
	mText.setOutlineThickness(3.0);
	centerOrigin(mText);
	mText.setPosition(280,480);
	introMusicPlayed = false;

	context.music->play(Music::MenuIntro, false);
}

void TitleState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.draw(mBackgroundSprite);

	if (mShowText)
	{
		window.draw(mText);
	}
}

bool TitleState::update(sf::Time dt)
{
	// This code sets up the menu intro music and then loop music which plays after the intro completes
	if (introMusicPlayed == false){
		Context context = getContext();

		if (!context.music->isPlaying()) {
			introMusicPlayed = true;
			context.music->play(Music::MenuLoop);
		}
	}

	mTextEffectTime += dt;
	if (mTextEffectTime >= sf::seconds(0.5f))
	{
		mShowText = !mShowText;
		mTextEffectTime = sf::Time::Zero;
	}
	return true;
}

bool TitleState::handleEvent(const sf::Event& event)
{
	//If any key is pressed, trigger the next screen
	if (event.type == sf::Event::KeyReleased)
	{
		requestStackPop();
		requestStackPush(States::Menu);
	}
	return true;
}