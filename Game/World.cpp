#include "World.hpp"
#include "EndGameVan.h"
#include "Projectile.hpp"
#include "Pickup.hpp"
#include "Foreach.hpp"
#include "StaticTextNode.h"
#include "ParticleNode.hpp"
#include "SoundNode.hpp"
#include "NetworkNode.hpp"
#include "Utility.hpp"
#include "StaticSprite.h"
#include "SoundPlayer.hpp"
#include "Wall.h"
#include "Collision.h"



#include <SFML/Graphics/RenderTarget.hpp>


#include <algorithm>
#include <cmath>
#include <limits>
#include <cmath>

int enemiesKilled;
int numEnemies;
extern int gameScore;
sf::Vector2f mouseTarget;

World::World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, bool networked)
	: mTarget(outputTarget)
	, mSceneTexture()
	, mWorldView(outputTarget.getDefaultView())
	, mTextures()
	, mFonts(fonts)
	, mSounds(sounds)
	, mSceneGraph()
	, mSceneLayers()
	, mWorldBounds(0.f, 0.f, 4856.f, 3735.f)
	//, mSpawnPosition(mWorldView.getSize().x / 2.f, mWorldBounds.height - mWorldView.getSize().y / 2.f)
	, mSpawnPosition(mWorldView.getSize().x / 2.f, mWorldView.getSize().y / 2.f)
	, mScrollSpeed(-300.f)
	, mScrollSpeedCompensation(1.1f)
	, mPlayerAircrafts()
	, mEnemySpawnPoints()
	, mActiveEnemies()
	, mNetworkedWorld(networked)
	, mNetworkNode(nullptr)
	, mFinishSprite(nullptr)
	, spawnOffset(0.f)
{
	enemiesKilled = 0;
	gameScore = 0;
	mSceneTexture.create(mTarget.getSize().x, mTarget.getSize().y);

	loadTextures();
	buildScene();
	// Prepare the view
	mWorldView.setCenter(mSpawnPosition);
}

void World::setWorldScrollCompensation(float compensation)
{
	mScrollSpeedCompensation = compensation;
}

void World::update(sf::Time dt)
{
	//if (enemiesKilled >= numEnemies) {
	//	createMoreEnemies(abs(spawnOffset));
	//	sortEnemies();
	//	enemiesKilled = 0;
	//}

	//if (mPlayerAircrafts.size() == 1) {
	//	FOREACH(Vehicle* player, mPlayerAircrafts) {
	//		if (player->mType == Vehicle::Type::Max) {
	//			justiceWin = true;
	//		}
	//		else {
	//			robbersWin = true;
	//		}
	//	}
	//}

	FOREACH(Vehicle* player, mPlayerAircrafts) {
		if (!player) {
			continue;
		}

		if (mPlayerAircrafts.size() == 1) {
			if (player->mType == Vehicle::Max) {
				justiceWin = true;
			}
		}

		if (player->followCamera) {
			mWorldView.setCenter(player->getPosition()); 
			break;
		}
	}

	//if (isSuddenDeath) {
	//	if (suddenDeathTextNode) {

	//		//TextNode->attachChild()

	//		sf::Vector2f center = mWorldView.getCenter();
	//		sf::Vector2f size = mWorldView.getSize();
	//		sf::Vector2f screenTop = sf::Vector2f(center.x, center.y - (size.y * .45f));
	//		suddenDeathTextNode->setPosition(center);
	//		//mSceneLayers[Background]->attachChild(suddenDeathText);
	//	}
	//}
	//if (!worldIndexSet) {
	//	if (mPlayerAircrafts.size() > 0) {
	//		worldIndex = mPlayerAircrafts.size() - 1;
	//		worldIndexSet = true;
	//	}
	//}
	//

	//if (worldIndexSet) {
	//	mWorldView.setCenter(mPlayerAircrafts[worldIndex]->getPosition());
	//}

	//FOREACH(Vehicle* a, mPlayerAircrafts) {
	//	break;
	//}
	
	//if (mPlayerAircrafts.size() > 0) {
	//	
	//	mWorldView.setCenter(mPlayerAircrafts[0]->getPosition());
	//}
	


	updateTimeSinceLastFrame();
	Decellerate(dt);

	// Setup commands to destroy entities, and guide missiles
	// destroyEntitiesOutsideView();
	// guideMissiles();

	// Forward commands to scene graph, adapt velocity (scrolling, diagonal correction)
	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);

	World::adaptPlayerVelocity();

	// Collision detection and response (may destroy entities)
	handleCollisions();

	// Remove aircrafts that were destroyed (World::removeWrecks() only destroys the entities, not the pointers in mPlayerAircraft)
	auto firstToRemove = std::remove_if(mPlayerAircrafts.begin(), mPlayerAircrafts.end(), std::mem_fn(&Vehicle::isMarkedForRemoval));


	mPlayerAircrafts.erase(firstToRemove, mPlayerAircrafts.end());

	// Remove all destroyed entities, create new ones
	mSceneGraph.removeWrecks();
	//spawnEnemies();

	// Regular update step, adapt position (correct if outside view)
	mSceneGraph.update(dt, mCommandQueue);
	//adaptPlayerPosition();
	updateMousePosition();
	updateSounds();
}

void World::draw()
{
	if (PostEffect::isSupported())
	{
		mSceneTexture.clear();
		mSceneTexture.setView(mWorldView);
		mSceneTexture.draw(mSceneGraph);
		mSceneTexture.display();
		mBloomEffect.apply(mSceneTexture, mTarget);
	}
	else
	{
		mTarget.setView(mWorldView);
		mTarget.draw(mSceneGraph);
	}
}

CommandQueue& World::getCommandQueue()
{
	return mCommandQueue;
}

Vehicle* World::getAircraft(int identifier) const
{
	FOREACH(Vehicle* a, mPlayerAircrafts)
	{
		if (a->getIdentifier() == identifier)
			return a;
	}

	return nullptr;
}

Vehicle* World::getLastPlayer() const
{
	FOREACH(Vehicle* a, mPlayerAircrafts)
	{
		return a;
		break;
	}

	return nullptr;
}

Vehicle* World::getJustice() const
{
	FOREACH(Vehicle* a, mPlayerAircrafts)
	{
		if (a->mType == Vehicle::Max) {
			return a;
		}
	}

	return nullptr;
}

void World::removeAircraft(int identifier)
{
	Vehicle* aircraft = getAircraft(identifier);
	if (aircraft)
	{
		aircraft->destroy();
		mPlayerAircrafts.erase(std::find(mPlayerAircrafts.begin(), mPlayerAircrafts.end(), aircraft));
	}
}

Vehicle* World::addJustice(int identifier)
{
	std::unique_ptr<Vehicle> player(new Vehicle(Vehicle::Max, mTextures, mFonts));
	player->setPosition(mWorldView.getCenter());
	player->setIdentifier(identifier);

	std::unique_ptr<StaticSprite> windowCharacter(new StaticSprite(StaticSprite::WindowCharacter, mTextures));
	windowCharacter->setPosition(sf::Vector2f(15.f,-5.f));

	std::unique_ptr<StaticSprite> characterGun(new StaticSprite(StaticSprite::Gun, mTextures));
	characterGun->setPosition(sf::Vector2f(0.f, -3.f));
	characterGun->setOrigin(0, 2);

	std::unique_ptr<StaticSprite> sirens(new StaticSprite(StaticSprite::Sirens, mTextures));
	sirens->setPosition(sf::Vector2f(0, -4.f));
	player->attachChild(std::move(sirens));

	windowCharacter->attachChild(std::move(characterGun));
	player->attachChild(std::move(windowCharacter));


	mPlayerAircrafts.push_back(player.get());

	mSceneLayers[UpperAir]->attachChild(std::move(player));
	return mPlayerAircrafts.back();
}

void World::setJustice(int playerIdentifier) {
	//std::vector<Vehicle*> a = mPlayerAircrafts;

	int index = 0;
	float offset = 500;

	 std::vector<sf::Color> colorIndexList = std::vector<sf::Color>{
		sf::Color::Red,
		sf::Color::Green,
		sf::Color::Blue,
		sf::Color::Magenta,
		sf::Color::Cyan,
		sf::Color::Red,
		sf::Color::Green,
		sf::Color::Blue,
		sf::Color::Magenta,
		sf::Color::Cyan,
		sf::Color::Red,
		sf::Color::Green,
		sf::Color::Blue,
		sf::Color::Magenta,
		sf::Color::Cyan,
	};

	std::vector<sf::Vector2f> spawnPosIndexList = std::vector<sf::Vector2f>{
		sf::Vector2f (2300, 700),
		sf::Vector2f (1240, 700),
		sf::Vector2f (3250, 700),
		sf::Vector2f (2300, 900),
		sf::Vector2f (1240, 900),
		sf::Vector2f (3250, 900),
		sf::Vector2f (2300, 1500),
		sf::Vector2f (1240, 1500),
		sf::Vector2f (3250, 1500),
		sf::Vector2f (2300, 2400),
		sf::Vector2f (1240, 2400),
		sf::Vector2f (3250, 2400),
		sf::Vector2f (2300, 2000),
		sf::Vector2f (1240, 2000),
		sf::Vector2f (3250, 2000)
	};
	

	FOREACH(Vehicle* v, mPlayerAircrafts) {

		if ((index > colorIndexList.size() - 1) || (index > spawnPosIndexList.size() -1)){
			index = 0;
		}

		v->mSprite.setColor(colorIndexList[index]);
		v->setPosition(spawnPosIndexList[index]);
		index++;
	}	
	
	Vehicle* Justice = getAircraft(playerIdentifier);
	Justice->resetType(Vehicle::Type::Max, mTextures);
	Justice->mSprite.setColor(sf::Color::White);

	std::unique_ptr<StaticSprite> sirens(new StaticSprite(StaticSprite::Sirens, mTextures));
	sirens->setPosition(sf::Vector2f(0, -4.f));
	Justice->attachChild(std::move(sirens));
	//std::unique_ptr<Vehicle> newplayer(new Vehicle(Vehicle::Max, mTextures, mFonts));

	//mSceneLayers[UpperAir]->attachChild(std::move(newplayer));

	//Justice = newplayer;

	//Justice->setPosition(0, 0);

}

void World::suddenDeath(int location) {
	isSuddenDeath = true;
	//std::unique_ptr<SceneNode> suddenDeathTextNodePtr(new SceneNode());

	
	FOREACH(Vehicle* v, mPlayerAircrafts) {
		std::unique_ptr<TextNode> text(new TextNode(mFonts, "SUDDEN DEATH"));
		text->mText.setFillColor(sf::Color::Red);
		v->attachChild(std::move(text));
	}
	//suddenDeathTextNodePtr->attachChild(std::move(text));
	//suddenDeathTextNodePtr->setPosition(mWorldView.getCenter());

	std::unique_ptr<EndGameVan> endGameVan(new EndGameVan(mTextures));

	if (location > 2) {
		location = 2;
	}

	std::vector<sf::Vector2f> vanLocationIndexList = std::vector<sf::Vector2f>{
		sf::Vector2f(2250, 1500),
		sf::Vector2f(2250, 1000),
		sf::Vector2f(2250, 2000),
	};

	endGameVan->setPosition(vanLocationIndexList[location]);
	
	//suddenDeathTextNode = suddenDeathTextNodePtr.get();
	//FOREACH(Vehicle* v, mPlayerAircrafts) {

	//	v->attachChild(std::move(text));
	//}

	//
	mSceneLayers[UpperAir]->attachChild(std::move(endGameVan));
	//mSceneLayers[UpperAir]->attachChild(std::move(suddenDeathTextNodePtr));
}

Vehicle* World::addRobber(int identifier)
{
	std::unique_ptr<Vehicle> player(new Vehicle(Vehicle::Biker, mTextures, mFonts));

	//player->setPosition(mWorldView.getCenter());
	//player->setPosition(0, -200);
	player->setIdentifier(identifier);
		
	mPlayerAircrafts.push_back(player.get());

	mSceneLayers[UpperAir]->attachChild(std::move(player));
	return mPlayerAircrafts.back();
}

void World::addToPlayerList(std::unique_ptr<Vehicle> player) {
	/*mPlayerAircrafts.push_back(player.get());*/
}

void World::createPickup(sf::Vector2f position, Pickup::Type type)
{
	std::unique_ptr<Pickup> pickup(new Pickup(type, mTextures));
	pickup->setPosition(position);
	pickup->setVelocity(0.f, 1.f);
	mSceneLayers[UpperAir]->attachChild(std::move(pickup));
}

bool World::pollGameAction(GameActions::Action& out)
{
	return mNetworkNode->pollGameAction(out);
}

void World::setCurrentBattleFieldPosition(float lineY)
{
	mWorldView.setCenter(mWorldView.getCenter().x, lineY - mWorldView.getSize().y / 2);
	mSpawnPosition.y = mWorldBounds.height;
}

void World::setWorldHeight(float height)
{
	mWorldBounds.height = height;
}

bool World::hasAlivePlayer() const
{
	return mPlayerAircrafts.size() > 0;
}

bool World::hasPlayerReachedEnd() const
{
	if (Vehicle* aircraft = getAircraft(1))
		return !mWorldBounds.contains(aircraft->getPosition());
	else
		return false;
}

void World::loadTextures()
{
	mTextures.load(Textures::Entities, "Media/Textures/Entities.png");
	mTextures.load(Textures::Jungle, "Media/Textures/Jungle.png");
	mTextures.load(Textures::InstructionsScreen, "Media/Textures/InstructionsScreen.png");
	mTextures.load(Textures::Explosion, "Media/Textures/Explosion.png");
	mTextures.load(Textures::PopText, "Media/Textures/PopText.png");
	mTextures.load(Textures::Particle, "Media/Textures/Particle.png");
	mTextures.load(Textures::Sirens, "Media/Textures/sirens.png");
	mTextures.load(Textures::FinishLine, "Media/Textures/FinishLine.png");
}

void World::adaptPlayerPosition()
{
	// Keep player's position inside the screen bounds, at least borderDistance units from the border
	sf::FloatRect viewBounds = getViewBounds();
	const float borderDistance = 40.f;

	FOREACH(Vehicle* aircraft, mPlayerAircrafts)
	{
		sf::Vector2f position = aircraft->getPosition();
		position.x = std::max(position.x, viewBounds.left + borderDistance);
		position.x = std::min(position.x, viewBounds.left + viewBounds.width - borderDistance);
		position.y = std::max(position.y, viewBounds.top + borderDistance);
		position.y = std::min(position.y, viewBounds.top + viewBounds.height - borderDistance);
		aircraft->setPosition(position);
	}
}

void World::adaptPlayerVelocity()
{
	//FOREACH(Vehicle* aircraft, mPlayerAircrafts)
	//{
	//	sf::Vector2f velocity = aircraft->getVelocity();

	//	// If moving diagonally, reduce velocity (to have always same velocity)
	//	if (velocity.x != 0.f && velocity.y != 0.f)
	//		aircraft->setVelocity(velocity / std::sqrt(2.f));

	//	// Add scrolling velocity
	//	// aircraft->accelerate(0.f, mScrollSpeed);
	//}
}

bool matchesCategories(SceneNode::Pair& colliders, Category::Type type1, Category::Type type2)
{
	unsigned int category1 = colliders.first->getCategory();
	unsigned int category2 = colliders.second->getCategory();

	// Make sure first pair entry has category type1 and second has type2
	if (type1 & category1 && type2 & category2)
	{
		return true;
	}
	else if (type1 & category2 && type2 & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}

void World::handleCollisions()
{
	std::set<SceneNode::Pair> collisionPairs;
	mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs);

	FOREACH(SceneNode::Pair pair, collisionPairs)
	{
		if (matchesCategories(pair, Category::PlayerAircraft, Category::EnemyAircraft))
		{
			auto& player = static_cast<Vehicle&>(*pair.first);
			auto& enemy = static_cast<Vehicle&>(*pair.second);

			if (enemy.mType != Vehicle::Helicopter) {
				// Collision: Player damage = enemy's remaining HP
				//player.damage(enemy.getHitpoints());
				enemy.destroy();
			}
		}

		//else if (matchesCategories(pair, Category::PlayerAircraft, Category::Pickup))
		//{
		//	auto& player = static_cast<Vehicle&>(*pair.first);
		//	auto& pickup = static_cast<Pickup&>(*pair.second);

		//	// Apply pickup effect to player, destroy projectile
		//	pickup.apply(player);
		//	pickup.destroy();
		//	player.playLocalSound(mCommandQueue, SoundEffect::CollectPickup);
		//}

		//else if (matchesCategories(pair, Category::EnemyAircraft, Category::AlliedProjectile)
		//	|| matchesCategories(pair, Category::PlayerAircraft, Category::EnemyProjectile))
		//{
		//	auto& aircraft = static_cast<Vehicle&>(*pair.first);
		//	auto& projectile = static_cast<Projectile&>(*pair.second);

		//	// Apply projectile damage to aircraft, destroy projectile
		//	aircraft.damage(projectile.getDamage());
		//	mSounds.play(SoundEffect::HitMarker);
		//	projectile.destroy();
		//}

		else if (matchesCategories(pair, Category::EnemyAircraft, Category::EndGameVan))
		{
			// Bad guys win
			FOREACH(Vehicle* player, mPlayerAircrafts) {
				if (player->mType == Vehicle::Max) {
					player->destroy();
					robbersWin = true;
				}
			}
		}

		else if (matchesCategories(pair, Category::Aircraft, Category::Wall))
		{
			auto& aircraft = static_cast<Vehicle&>(*pair.first);
			auto& wall = static_cast<Wall&>(*pair.second);

			// Play sound and move player to closest safe position
			//mSounds.play(SoundEffect::HitMarker);

			sf::Vector2f wallPoint = wall.getPosition();
			sf::Vector2f aircraftPoint = aircraft.getPosition();

			float distToLeft = wallPoint.x - aircraftPoint.x;
			float distToRight = aircraftPoint.x - wallPoint.x;
			float distToTop = wallPoint.y - aircraftPoint.y;
			float distToBottom = aircraftPoint.y - wallPoint.y;



			int smallest = 2;

			//if (distToLeft > 0 && distToRight > 0 && distToTop > 0 && distToBottom > 0)
			if(Collision::BoundingBoxTest(aircraft.mSprite, wall.mSprite))
			{
					
				bool leftCollision;
				bool rightCollision;
				bool topCollision;
				bool bottomCollision;

				sf::FloatRect playerRect = aircraft.getBoundingRect();
				int playerLeft = playerRect.left;
				int playerRight = playerLeft + playerRect.width;
				int playerTop = playerRect.top;
				int playerBottom = playerTop + playerRect.height;

				sf::FloatRect rect = wall.getBoundingRect();
				int wallLeft = rect.left;
				int wallRight = wallLeft + rect.width;
				int wallTop = rect.top;
				int wallBottom = wallTop + rect.height;

				leftCollision = playerLeft < wallLeft && playerRight >= wallLeft;
				rightCollision = playerRight > wallRight && playerLeft <= wallRight;
				topCollision = playerTop < wallTop && playerBottom >= wallTop;
				bottomCollision = playerBottom > wallBottom && playerTop <= wallBottom;

				float xVelocity = aircraft.getVelocity().x;
				float yVelocity = aircraft.getVelocity().y;

				if (leftCollision) {
					if (topCollision) {
						if (abs(yVelocity) > abs(xVelocity)) {
							// Keep travelling upwards
							aircraft.setVelocity(0.f, yVelocity);
						}
						//else {
						//	// Keep travelling left
						//	aircraft.setVelocity(xVelocity, 0.f);
						//}
					}
					else if (bottomCollision) {
						if (abs(yVelocity) > abs(xVelocity)) {
							// keep travelling down
							aircraft.setVelocity(0.f, yVelocity);
						}
						//else {
						//	// keep travelling left
						//	aircraft.setVelocity(xVelocity, 0.f);
						//}
					}
					else {
						// Keep travelling up/down
						aircraft.setVelocity(-20.f, yVelocity);
					}
				}
				else if (rightCollision) {
					if (topCollision) {
						if (abs(yVelocity) > abs(xVelocity)) {
							// Keep travelling upwards
							aircraft.setVelocity(0.f, yVelocity);
						}
						//else {
						//	// Keep travelling right
						//	aircraft.setVelocity(xVelocity, 0.f);
						//}
					}
					else if (bottomCollision) {
						if (abs(yVelocity) > abs(xVelocity)) {
							// keep travelling down
							aircraft.setVelocity(0.f, yVelocity);
						}
						//else {
						//	// keep travelling right
						//	aircraft.setVelocity(xVelocity, 0.f);
						//}
					}
					else {
						// Keep travelling up/down
						aircraft.setVelocity(20.f, yVelocity);
					}
				}
				else if (topCollision) {
					if (leftCollision) {
						if (abs(yVelocity) > abs(xVelocity)) {
							// Keep travelling left
							aircraft.setVelocity(xVelocity, 0);
						}
						//else {
						//	// Keep travelling left
						//	aircraft.setVelocity(xVelocity, 0.f);
						//}
					}
					else if (rightCollision) {
						if (abs(yVelocity) > abs(xVelocity)) {
							// keep travelling right
							aircraft.setVelocity(xVelocity, 0);
						}
						//else {
						//	// keep travelling left
						//	aircraft.setVelocity(xVelocity, 0.f);
						//}
					}
					else {
						// Keep travelling left/right
						aircraft.setVelocity(xVelocity, -20);
					}
				}
				else if (bottomCollision) {
					if (leftCollision) {
						if (abs(yVelocity) > abs(xVelocity)) {
							// Keep travelling left
							aircraft.setVelocity(xVelocity, 0.f);
						}
						//else {
						//	// Keep travelling left
						//	aircraft.setVelocity(xVelocity, 0.f);
						//}
					}
					else if (rightCollision) {
						if (abs(yVelocity) > abs(xVelocity)) {
							// keep travelling right
							aircraft.setVelocity(xVelocity, 0.f);
						}
						//else {
						//	// keep travelling left
						//	aircraft.setVelocity(xVelocity, 0.f);
						//}
					}
					else {
						// Keep travelling left/right
						aircraft.setVelocity(xVelocity, 20.f);
					}
				}
			}
		}
	}
}

void World::addWalls()
{
	createWall(sf::Vector2f(1250, 550), sf::Vector2f(5, 3), Wall::Type::Straight);
	createWall(sf::Vector2f(3250, 550), sf::Vector2f(5, 3), Wall::Type::Straight);
	createWall(sf::Vector2f(1250, 3530), sf::Vector2f(5, 3), Wall::Type::Straight);
	createWall(sf::Vector2f(3250, 3530), sf::Vector2f(5, 3), Wall::Type::Straight);
	//top
	createWall(sf::Vector2f(2300, 440), sf::Vector2f(60, 3), Wall::Type::Straight);
	//bottom
	createWall(sf::Vector2f(2300, 3640), sf::Vector2f(60, 3), Wall::Type::Straight);
	//right 1st block
	createWall(sf::Vector2f(2603, 658), sf::Vector2f(11, 3), Wall::Type::Straight);
	createWall(sf::Vector2f(1933, 658), sf::Vector2f(11, 3), Wall::Type::Straight);
	//right 1st blockbotom
	createWall(sf::Vector2f(2603, 3400), sf::Vector2f(11, 3), Wall::Type::Straight);
	createWall(sf::Vector2f(1933, 3400), sf::Vector2f(11, 3), Wall::Type::Straight);
	//right 2nd block
	createWall(sf::Vector2f(2603, 1308), sf::Vector2f(11, 20), Wall::Type::Straight);
	createWall(sf::Vector2f(1933, 1308), sf::Vector2f(11, 20), Wall::Type::Straight);
	//right 2nd block
	createWall(sf::Vector2f(2603, 2748), sf::Vector2f(11, 20), Wall::Type::Straight);
	createWall(sf::Vector2f(1933, 2748), sf::Vector2f(11, 20), Wall::Type::Straight);
	//right 3rd block
	createWall(sf::Vector2f(2603, 1868), sf::Vector2f(11, 3), Wall::Type::Straight);
	createWall(sf::Vector2f(1933, 1868), sf::Vector2f(11, 3), Wall::Type::Straight);
	//bottom right 3rd block
	createWall(sf::Vector2f(2603, 2198), sf::Vector2f(11, 3), Wall::Type::Straight);
	createWall(sf::Vector2f(1933, 2198), sf::Vector2f(11, 3), Wall::Type::Straight);
	//right 4th block
	createWall(sf::Vector2f(3093, 728), sf::Vector2f(6, 18), Wall::Type::Straight);
	createWall(sf::Vector2f(1448, 728), sf::Vector2f(6, 18), Wall::Type::Straight);
	//right 4th block bottom
	createWall(sf::Vector2f(3093, 3338), sf::Vector2f(6, 18), Wall::Type::Straight);
	createWall(sf::Vector2f(1448, 3338), sf::Vector2f(6, 18), Wall::Type::Straight);
	//right 4th block
	createWall(sf::Vector2f(3093, 1590), sf::Vector2f(6, 17), Wall::Type::Straight);
	createWall(sf::Vector2f(1448, 1590), sf::Vector2f(6, 17), Wall::Type::Straight);
	//right 4th block
	createWall(sf::Vector2f(3093, 2490), sf::Vector2f(6, 17), Wall::Type::Straight);
	createWall(sf::Vector2f(1448, 2490), sf::Vector2f(6, 17), Wall::Type::Straight);
	

	//right side
	createWall(sf::Vector2f(3410, 2030), sf::Vector2f(3, 80), Wall::Type::Straight);
	//left side
	createWall(sf::Vector2f(1124, 2030), sf::Vector2f(3, 80), Wall::Type::Straight);

}

void World::createWall(sf::Vector2f position, sf::Vector2f scale, Wall::Type type)
{
	std::unique_ptr<Wall> wall(new Wall(type, mTextures));
	wall->setPosition(position);
	wall->setScale(scale);
	mSceneLayers[UpperAir]->attachChild(std::move(wall));
}

void World::updateSounds()
{
	sf::Vector2f listenerPosition;

	// 0 players (multiplayer mode, until server is connected) -> view center
	if (mPlayerAircrafts.empty())
	{
		listenerPosition = mWorldView.getCenter();
	}

	// 1 or more players -> mean position between all aircrafts
	else
	{
		FOREACH(Vehicle* aircraft, mPlayerAircrafts)
			listenerPosition += aircraft->getWorldPosition();

		listenerPosition /= static_cast<float>(mPlayerAircrafts.size());
	}

	// Set listener's position
	mSounds.setListenerPosition(listenerPosition);

	// Remove unused sounds
	mSounds.removeStoppedSounds();
}

void World::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < LayerCount; ++i)
	{
		Category::Type category = (i == LowerAir) ? Category::SceneAirLayer : Category::None;

		SceneNode::Ptr layer(new SceneNode(category));
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	// Prepare the tiled background
	sf::Texture& jungleTexture = mTextures.get(Textures::Jungle);
	jungleTexture.setRepeated(true);

	float viewHeight = mWorldView.getSize().y;
	sf::IntRect textureRect(mWorldBounds);
	textureRect.height += static_cast<int>(viewHeight);

	// Add the background sprite to the scene
	std::unique_ptr<SpriteNode> jungleSprite(new SpriteNode(jungleTexture, textureRect));
	jungleSprite->setPosition(mWorldBounds.left, mWorldBounds.top - viewHeight*1.2f);
	mSceneLayers[Background]->attachChild(std::move(jungleSprite));

	// Add particle node to the scene
	std::unique_ptr<ParticleNode> smokeNode(new ParticleNode(Particle::Smoke, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(smokeNode));

	// Add propellant particle node to the scene
	std::unique_ptr<ParticleNode> propellantNode(new ParticleNode(Particle::Propellant, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(propellantNode));

	// Add bullet trace particle node to the scene
	std::unique_ptr<ParticleNode> dirtNode(new ParticleNode(Particle::Dirt, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(dirtNode));

	//Add sound effect node
	std::unique_ptr<SoundNode> soundNode(new SoundNode(mSounds));
	mSceneGraph.attachChild(std::move(soundNode));

	// Add network node, if necessary
	if (mNetworkedWorld)
	{
		std::unique_ptr<NetworkNode> networkNode(new NetworkNode());
		mNetworkNode = networkNode.get();
		mSceneGraph.attachChild(std::move(networkNode));
	}

	// Add enemy aircraft
	//addEnemies();
	addWalls();
}

void World::createMoreEnemies(int offset = 0) {
	/*addEnemy(Vehicle::MuscleCar, 70.f, 800.f + offset);

	addEnemy(Vehicle::Biker, -200.f, 1000.f + offset);

	addEnemy(Vehicle::Helicopter, 0.f, 1000.f + offset);

	addEnemy(Vehicle::MuscleCar, -100.f, 1800.f + offset);

	addEnemy(Vehicle::MuscleCar, 400.f, 2700.f + offset);

	addEnemy(Vehicle::MuscleCar, 0.f, 3400.f + offset);

	addEnemy(Vehicle::MuscleCar, 200.f, 3700.f + offset);

	addEnemy(Vehicle::Helicopter, 0.f, 4400.f + offset);

	addEnemy(Vehicle::MuscleCar, 400.f, 5000.f + offset);

	addEnemy(Vehicle::MuscleCar, 200.f, 5500.f + offset);

	addEnemy(Vehicle::MuscleCar, 0.f, 5000.f + offset);

	addEnemy(Vehicle::Biker, 200.f, 5500.f + offset);

	numEnemies = 12;*/
}

void World::addEnemies()
{
	if (mNetworkedWorld)
		return;

	// Add enemies to the spawn point container
	createMoreEnemies();


	// Sort all enemies according to their y value, such that lower enemies are checked first for spawning
	sortEnemies();
}

void World::sortEnemies()
{
	// Sort all enemies according to their y value, such that lower enemies are checked first for spawning
	std::sort(mEnemySpawnPoints.begin(), mEnemySpawnPoints.end(), [](SpawnPoint lhs, SpawnPoint rhs)
	{
		return lhs.y < rhs.y;
	});
}

void World::Decellerate(sf::Time dt)
{
	sf::Vector2f velocityDecelleration = sf::Vector2f(-1, -1);

	//if (!mDecellerateCars) {
	//	return;
	//}
	//mDecellerateCars = false;

		//sf::Vector2f velocity = mPlayerAircrafts[0]->getVelocity();
		//if (velocity.x < 0) {
		//	velocityDecelleration.x *= -1;
		//}


		//if (velocity.y < 0) {
		//	velocityDecelleration.y *= -1;
		//}


		//mPlayerAircrafts[0]->accelerate(velocityDecelleration);
		//FOREACH(Vehicle* a, mPlayerAircrafts) {
		//	a->accelerate(velocityDecelleration);
		//}
	
	/*FOREACH(Vehicle* a, mPlayerAircrafts) {
		sf::Vector2f velocity = a->getVelocity();
		if (velocity.x > 0) {
			velocity.x = velocity.x -  velocityDecelleration;
		}
		else if (velocity.y < 0) {
			velocity.x = velocity.x +  velocityDecelleration;
		}

		if (velocity.y > 0) {
			velocity.y = velocity.y - velocityDecelleration;
		}
		else if(velocity.y < 0){
			velocity.y = velocity.y + velocityDecelleration;
		}

		a->setVelocity(velocity.x, velocity.y);
	}	*/
}

void World::updateTimeSinceLastFrame()
{
	if (deltaClock.getElapsedTime().asSeconds() > 1) {
		mDecellerateCars = true;
		deltaClock.restart();
	}
}


void World::addEnemy(Vehicle::Type type, float relX, float relY)
{
	SpawnPoint spawn(type, mSpawnPosition.x + relX, mSpawnPosition.y - relY);
	mEnemySpawnPoints.push_back(spawn);
}

void World::spawnEnemies()
{
	float top = getBattlefieldBounds().top;
	// Spawn all enemies entering the view area (including distance) this frame
	while (!mEnemySpawnPoints.empty() && (mEnemySpawnPoints.back().y) > (getBattlefieldBounds().top))
	{
		SpawnPoint spawn = mEnemySpawnPoints.back();

		std::unique_ptr<Vehicle> enemy(new Vehicle(spawn.type, mTextures, mFonts));

		if (spawn.type == Vehicle::Helicopter || spawn.type == Vehicle::Biker) {
			enemy->setPosition(spawn.x, getBattlefieldBounds().top + 100);
			enemy->setRotation(180.f);

		}
		else {
			enemy->setPosition(spawn.x, getBattlefieldBounds().top + getBattlefieldBounds().height + 100);
		}



		if (mNetworkedWorld) enemy->disablePickups();

		mSceneLayers[UpperAir]->attachChild(std::move(enemy));

		// Enemy is spawned, remove from the list to spawn
		mEnemySpawnPoints.pop_back();
	}
}

void World::destroyEntitiesOutsideView()
{
	Command command;
	command.category = Category::Projectile | Category::EnemyAircraft;
	command.action = derivedAction<Entity>([this](Entity& e, sf::Time)
	{
		Vehicle& a = static_cast<Vehicle&>(e);

		if (a.getMaxSpeed() < 0) {
			float y = a.getWorldPosition().y;
			float top = getBattlefieldBounds().top;
			if (a.getWorldPosition().y < getBattlefieldBounds().top) {
				e.remove();
			}
			return;
		};


		if (!getBattlefieldBounds().intersects(e.getBoundingRect())) {
			e.remove();
		}

	});

	mCommandQueue.push(command);
}

void World::updateMousePosition() {
	mouseTarget = mTarget.mapPixelToCoords(MousePos, mWorldView);

	Command playerGuider;
	playerGuider.category = Category::EnemyProjectile;
	playerGuider.action = derivedAction<Projectile>([this](Projectile& missile, sf::Time)
	{
		if (missile.guideSet) {
			return;
		}

		if (missile.isPlayerFollower()) {

			FOREACH(Vehicle* player, mPlayerAircrafts)
			{
				missile.guideTowards(player->getWorldPosition() + (player->getVelocity() * 50.f));
				missile.guideSet = true;
				break;
			}
		}

	});

	// Setup command that guides all missiles to the enemy which is currently closest to the player
	Command mouseGuider;
	mouseGuider.category = Category::AlliedProjectile;
	mouseGuider.action = derivedAction<Projectile>([this](Projectile& missile, sf::Time)
	{

		
		if (missile.guideSet) {
			return;
		}

		if (missile.isMouseFollower()) {
			missile.guideTowards(mouseTarget);
			missile.guideSet = true;
		}
	});

	// Push commands, reset active enemies
	mCommandQueue.push(mouseGuider);
	mCommandQueue.push(playerGuider);
}


sf::FloatRect World::getViewBounds() const
{
	return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize() / 2.f, mWorldView.getSize());
}

sf::FloatRect World::getBattlefieldBounds() const
{
	// Return view bounds + some area at top, where enemies spawn
	sf::FloatRect bounds = getViewBounds();
	bounds.top -= 100.f;
	bounds.height += 100.f;

	return bounds;
}
