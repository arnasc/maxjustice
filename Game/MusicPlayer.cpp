#include "MusicPlayer.hpp"

extern bool musicMuted;

MusicPlayer::MusicPlayer()
	:mMusic()
	, mFilenames()
	, mVolume(50.f)
{
	mFilenames[Music::MenuTheme] = "Media/Music/MenuTheme.ogg";
	mFilenames[Music::MissionTheme] = "Media/Music/MissionTheme.ogg";
	mFilenames[Music::MenuIntro] = "Media/Music/MenuIntro.ogg";
	mFilenames[Music::MenuLoop] = "Media/Music/MenuLoop.ogg";


}

void MusicPlayer::play(Music::ID theme)
{
	play(theme, true);
}


void MusicPlayer::play(Music::ID theme, bool loop)
{
	std::string filename = mFilenames[theme];
	if (!mMusic.openFromFile(filename))
	{
		throw std::runtime_error("Music " + filename + " could not be loaded");
	}
	mMusic.setVolume(mVolume);
	if (musicMuted) {
		mMusic.setVolume(0);
	}
	mMusic.setLoop(loop);
	mMusic.play();
}

bool MusicPlayer::isPlaying() {

	return (mMusic.getStatus() == mMusic.Playing);
}

void MusicPlayer::stop()
{
	mMusic.stop();
}

void MusicPlayer::setVolume(float volume)
{
	mVolume = volume;
}

void MusicPlayer::setPaused(bool paused)
{
	if (paused)
	{
		mMusic.pause();
	}
	else
	{
		mMusic.play();
	}
}