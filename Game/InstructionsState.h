#pragma once
#include "State.hpp"
#include "Container.hpp"
#include "Button.hpp"
#include "Label.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include <array>


class InstructionsState : public State
{
public:
	InstructionsState(StateStack& stack, Context context);

	virtual void					draw();
	virtual bool					update(sf::Time dt);
	virtual bool					handleEvent(const sf::Event& event);

private:

private:
	sf::Sprite											mBackgroundSprite;
	GUI::Container										mGUIContainer;
};