#include "MenuState.hpp"
#include "Utility.hpp"
#include "Button.hpp"
#include "ResourceHolder.hpp"
#include "MusicPlayer.hpp"
#include "SoundPlayer.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

extern bool introMusicPlayed;
MenuState::MenuState(StateStack& stack, Context context)
	: State(stack, context)
	, mGUIContainer()
{
	sf::Texture& texture = context.textures->get(Textures::TitleScreen);
	mBackgroundSprite.setTexture(texture);

	auto playButton = std::make_shared<GUI::Button>(context);
	playButton->setPosition(0, 290);
	playButton->setText("Play");
	playButton->changeTexture(GUI::Button::PlayNormal);
	playButton->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(States::Game);
	});

	auto hostPlayButton = std::make_shared<GUI::Button>(context);
	hostPlayButton->setPosition(0, 365);
	hostPlayButton->setText("Host");
	hostPlayButton->changeTexture(GUI::Button::HostNormal);
	hostPlayButton->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(States::HostGame);
	});

	auto joinPlayButton = std::make_shared<GUI::Button>(context);
	joinPlayButton->setPosition(0, 445);
	joinPlayButton->setText("Join");
	joinPlayButton->changeTexture(GUI::Button::JoinNormal);
	joinPlayButton->setCallback([this]()
	{
		requestStackPop();
		requestStackPush(States::JoinGame);
	});

	auto settingsButton = std::make_shared<GUI::Button>(context);
	settingsButton->setPosition(0, 525);
	settingsButton->setText("Settings");
	settingsButton->changeTexture(GUI::Button::SettingsNormal);
	settingsButton->setCallback([this]()
	{
		requestStackPush(States::Settings);
	});

	auto instructionsButton = std::make_shared<GUI::Button>(context);
	instructionsButton->setPosition(0, 605);
	instructionsButton->setText("Instructions");
	instructionsButton->changeTexture(GUI::Button::InstructionsNormal);
	instructionsButton->setCallback([this]()
	{
		requestStackPush(States::Instructions);
	});

	auto exitButton = std::make_shared<GUI::Button>(context);
	exitButton->setPosition(0, 685);
	exitButton->setText("Quit");
	exitButton->changeTexture(GUI::Button::QuitNormal);
	exitButton->setCallback([this]()
	{
		requestStackPop();
	});

	mGUIContainer.pack(playButton);
	mGUIContainer.pack(hostPlayButton);
	mGUIContainer.pack(joinPlayButton);
	mGUIContainer.pack(settingsButton);
	mGUIContainer.pack(instructionsButton);
	mGUIContainer.pack(exitButton);

	//Play menu theme
	//context.music->play(Music::MenuLoop);
	context.sounds->play(SoundEffect::VoiceChewGum);
}

void MenuState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.setView(window.getDefaultView());

	window.draw(mBackgroundSprite);
	window.draw(mGUIContainer);
}

bool MenuState::update(sf::Time)
{
	if (introMusicPlayed == false) {
		Context context = getContext();

		if (!context.music->isPlaying()) {
			introMusicPlayed = true;
			context.music->play(Music::MenuLoop);
		}
	}

	return true;
}

bool MenuState::handleEvent(const sf::Event& event)
{
	mGUIContainer.handleEvent(event);
	return false;
}

