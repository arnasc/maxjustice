#include "StaticTextNode.h"
#include "Utility.hpp"

#include <SFML/Graphics/RenderTarget.hpp>


StaticTextNode::StaticTextNode(const FontHolder& fonts, const std::string& text) : TextNode(fonts, text)
{
	mText.setFont(fonts.get(Fonts::Main));
	mText.setCharacterSize(20);
	setString(text);
}

void StaticTextNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mText, states);
}

void StaticTextNode::setString(const std::string& text)
{
	mText.setString(text);
	centerOrigin(mText);
}