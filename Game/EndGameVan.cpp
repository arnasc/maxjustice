#include "EndGameVan.h"
#include "DataTables.hpp"
#include "Category.hpp"
#include "CommandQueue.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
#include "EmitterNode.hpp"


#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <cmath>
#include <cassert>


namespace
{
	const std::vector<EndGameVanData> Table = initializeEndGameVanData();
}

EndGameVan::EndGameVan(const TextureHolder& textures)
	: Entity(10)
	, mSprite(textures.get(Table[0].texture), Table[0].textureRect)
{
	centerOrigin(mSprite);
}

unsigned int EndGameVan::getCategory() const
{
	return Category::EndGameVan;
}

sf::FloatRect EndGameVan::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

void EndGameVan::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}