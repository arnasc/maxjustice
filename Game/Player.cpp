#include "Player.hpp"
#include "CommandQueue.hpp"
#include "Aircraft.hpp"
#include "Foreach.hpp"
#include "NetworkProtocol.hpp"

#include <SFML/Network/Packet.hpp>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include <algorithm>

using namespace std::placeholders;


int Player::mNumPlayers = 0;
bool Player::mJusticeExists = false;

struct AircraftMover
{
	AircraftMover(float vx, float vy, int identifier)
		: velocity(vx, vy), aircraftID(identifier)
	{
	}
	 
	void operator() (Vehicle& aircraft, sf::Time) const
	{
		float radians = M_PI / 180;
		float radianAngle = (aircraft.getRotation()-90) * radians;

		sf::Vector2f aircraftVelocity = aircraft.getVelocity();
		sf::Vector2f unitDirection = sf::Vector2f(std::cos(radianAngle), std::sin(radianAngle));

		if (abs(aircraftVelocity.x) > aircraft.getMaxSpeed()) 
		{
			unitDirection.x = 0;
		}
		if (abs(aircraftVelocity.y) > aircraft.getMaxSpeed()) 
		{
			unitDirection.y = 0;
		}


		// Velocity.y is our up key provinding forwards momentum, apply to both unit directions
		unitDirection.x = unitDirection.x * velocity.y;
		unitDirection.y = unitDirection.y * velocity.y;

		if (aircraft.getIdentifier() == aircraftID) 
		{
			//sf::Vector2f velandAcc = velocity + aircraft.getAcceleration();
			unitDirection.x *= aircraft.getAcceleration();
			unitDirection.y *= aircraft.getAcceleration();

			aircraft.accelerate(unitDirection);
		}
			
	}

	sf::Vector2f velocity;
	int aircraftID;
};

struct AircraftRotator
{
	AircraftRotator(float angle, int identifier)
		: angle(angle), aircraftID(identifier)
	{
	}

	void operator() (Vehicle& aircraft, sf::Time) const
	{
		if (aircraft.getIdentifier() == aircraftID) {
			aircraft.rotate(angle);
		}
			
	}

	float angle;
	int aircraftID;
};

struct AircraftFireTrigger
{
	AircraftFireTrigger(int identifier)
		: aircraftID(identifier)
	{
	}

	void operator() (Vehicle& aircraft, sf::Time) const
	{
		if (aircraft.getIdentifier() == aircraftID)
			aircraft.fire();
	}

	int aircraftID;
};

struct AircraftMouseFireTrigger
{
	AircraftMouseFireTrigger(int identifier)
		: aircraftID(identifier)
	{
	}

	void operator() (Vehicle& aircraft, sf::Time) const
	{
		if (aircraft.getIdentifier() == aircraftID)
			aircraft.mouseFire();
	}

	int aircraftID;
};

struct AircraftMissileTrigger
{
	AircraftMissileTrigger(int identifier)
		: aircraftID(identifier)
	{
	}

	void operator() (Vehicle& aircraft, sf::Time) const
	{
		if (aircraft.getIdentifier() == aircraftID)
			aircraft.launchMissile();
	}

	int aircraftID;
};

Player::Player(sf::TcpSocket* socket, sf::Int32 identifier, const KeyBinding* binding, sf::Vector2f spawnLocation)
	: mKeyBinding(binding)
	, mCurrentMissionStatus(MissionRunning)
	, mIdentifier(identifier)
	, mSocket(socket)
	, mPlayerType(Player::PlayerType::Robber)
	, spawnLocation(spawnLocation)
{
	// Don't set initial action bindings til the game begins
	//
	Initialize();
	// Assign all categories to player's aircraft

}

void Player::Initialize() {
	initializeActions();

	FOREACH(auto& pair, mActionBinding)
	{
		if (mPlayerType == PlayerType::Robber) {
			pair.second.category = Category::EnemyAircraft;
		}
		
		if (mPlayerType == PlayerType::MaxJustice) {
			pair.second.category = Category::PlayerAircraft;
		}
	}
}


void Player::handleEvent(const sf::Event& event, CommandQueue& commands)
{
	if (event.type == sf::Event::KeyPressed)
	{
		Action action;
		if (mKeyBinding && mKeyBinding->checkAction(event.key.code, action) && !isRealtimeAction(action))
		{
			// Network connected -> send event over network
			if (mSocket)
			{
				sf::Packet packet;
				packet << static_cast<sf::Int32>(Client::PlayerEvent);
				packet << mIdentifier;
				packet << static_cast<sf::Int32>(action);
				mSocket->send(packet);
			}

			// Network disconnected -> local event
			else
			{
				commands.push(mActionBinding[action]);
			}
		}
	}
	// Realtime change (network connected)
	if ((event.type == sf::Event::KeyPressed || event.type == sf::Event::KeyReleased) && mSocket)
	{
		Action action;
		if (mKeyBinding && mKeyBinding->checkAction(event.key.code, action) && isRealtimeAction(action))
		{
			// Send realtime change over network
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Client::PlayerRealtimeChange);
			packet << mIdentifier;
			packet << static_cast<sf::Int32>(action);
			packet << (event.type == sf::Event::KeyPressed);
			mSocket->send(packet);
		}
	}
}

bool Player::isLocal() const
{
	// No key binding means this player is remote
	return mKeyBinding != nullptr;
}

void Player::disableAllRealtimeActions()
{
	FOREACH(auto& action, mActionProxies)
	{
		sf::Packet packet;
		packet << static_cast<sf::Int32>(Client::PlayerRealtimeChange);
		packet << mIdentifier;
		packet << static_cast<sf::Int32>(action.first);
		packet << false;
		mSocket->send(packet);
	}
}

void Player::handleRealtimeInput(CommandQueue& commands)
{
	// Check if this is a networked game and local player or just a single player game
	if ((mSocket && isLocal()) || !mSocket)
	{
		// Lookup all actions and push corresponding commands to queue
		std::vector<Action> activeActions = mKeyBinding->getRealtimeActions();
		FOREACH(Action action, activeActions)
			commands.push(mActionBinding[action]);
	}
}

void Player::handleRealtimeNetworkInput(CommandQueue& commands)
{
	if (mSocket && !isLocal())
	{
		// Traverse all realtime input proxies. Because this is a networked game, the input isn't handled directly
		FOREACH(auto pair, mActionProxies)
		{
			if (pair.second && isRealtimeAction(pair.first))
				commands.push(mActionBinding[pair.first]);
		}
	}
}

void Player::handleNetworkEvent(Action action, CommandQueue& commands)
{
	commands.push(mActionBinding[action]);
}

void Player::handleNetworkRealtimeChange(Action action, bool actionEnabled)
{
	mActionProxies[action] = actionEnabled;
}

void Player::setMissionStatus(MissionStatus status)
{
	mCurrentMissionStatus = status;
}

Player::MissionStatus Player::getMissionStatus() const
{
	return mCurrentMissionStatus;
}

void Player::initializeActions()
{
	mActionBinding[PlayerAction::MoveLeft].action = derivedAction<Vehicle>(AircraftRotator(-3, mIdentifier));
	mActionBinding[PlayerAction::MoveRight].action = derivedAction<Vehicle>(AircraftRotator(+3, mIdentifier));
	mActionBinding[PlayerAction::MoveUp].action = derivedAction<Vehicle>(AircraftMover(0, 1, mIdentifier));
	mActionBinding[PlayerAction::MoveDown].action = derivedAction<Vehicle>(AircraftMover(0, -1, mIdentifier));
	mActionBinding[PlayerAction::MouseFire].action = derivedAction<Vehicle>(AircraftMouseFireTrigger(mIdentifier));
	mActionBinding[PlayerAction::LaunchMissile].action = derivedAction<Vehicle>(AircraftMissileTrigger(mIdentifier));
}
