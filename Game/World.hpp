#pragma once
#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"
#include "SceneNode.hpp"
#include "SpriteNode.hpp"
#include "Aircraft.hpp"
#include "CommandQueue.hpp"
#include "Command.hpp"
#include "Pickup.hpp"
#include "BloomEffect.hpp"
#include "SoundPlayer.hpp"
#include "NetworkProtocol.hpp"
#include "Wall.h"

#include <SFML/System/Clock.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <array>
#include <queue>
extern sf::Vector2i MousePos;
//Foward declaration
namespace sf
{
	class RenderTarget;
}

class NetworkNode;

class World : private sf::NonCopyable
{
public:
	explicit World(sf::RenderTarget& window, FontHolder& font, SoundPlayer& sounds, bool networked = false);
	void update(sf::Time dt);
	void draw();

	bool robbersWin = false;
	bool justiceWin = false;

	sf::FloatRect getViewBounds() const;
	CommandQueue& getCommandQueue();
	Vehicle* addJustice(int identifier);
	Vehicle* addRobber(int identifier);
	void removeAircraft(int identifier);
	void setCurrentBattleFieldPosition(float lineY);
	void setWorldHeight(float height);

	void addEnemy(Vehicle::Type type, float relX, float relY);
	void sortEnemies();
	void setJustice(int playerIdentifier);
	void suddenDeath(int location);

	int worldIndex;
	bool worldIndexSet = false;
	bool isSuddenDeath = false;
	bool hasAlivePlayer() const;
	bool hasPlayerReachedEnd() const;

	void setWorldScrollCompensation(float compensation);

	Vehicle* getAircraft(int identifier) const;
	Vehicle* getJustice() const;
	Vehicle* getLastPlayer() const;
	sf::FloatRect getBattlefieldBounds() const;

	void createPickup(sf::Vector2f position, Pickup::Type type);
	void createWall(sf::Vector2f position, sf::Vector2f scale, Wall::Type type);
	bool pollGameAction(GameActions::Action& out);

private:
	void Decellerate(sf::Time dt);
	void updateTimeSinceLastFrame();
	void loadTextures();
	void adaptPlayerPosition();
	void adaptPlayerVelocity();
	void handleCollisions();
	void updateSounds();
	void addToPlayerList(std::unique_ptr<Vehicle> player);
	void buildScene();
	void createMoreEnemies(int offset);
	void addEnemies();
	void addWalls();
	void spawnEnemies();
	void destroyEntitiesOutsideView();
	void updateMousePosition();


	//void guideMissiles();

private:
	enum Layer
	{
		Background,
		LowerAir,
		UpperAir,
		LayerCount
	};

	struct SpawnPoint
	{
		SpawnPoint(Vehicle::Type type, float x, float y)
			: type(type)
			, x(x)
			, y(y)
		{
		}

		Vehicle::Type type;
		float x;
		float y;
	};



private:
	float								spawnOffset;


	bool								mDecellerateCars = false;
	sf::RenderTarget&					mTarget;
	sf::RenderTexture					mSceneTexture;
	sf::View							mWorldView;
	TextNode*							mScoreDisplay;
	TextureHolder						mTextures;
	FontHolder&							mFonts;
	SoundPlayer&						mSounds;

	SceneNode							mSceneGraph;
	std::array<SceneNode*, LayerCount>	mSceneLayers;
	CommandQueue						mCommandQueue;

	sf::FloatRect						mWorldBounds;
	sf::Vector2f						mSpawnPosition;
	float								mScrollSpeed;
	float								mScrollSpeedCompensation;
	std::vector<Vehicle*>				mPlayerAircrafts;

	std::vector<SpawnPoint>				mEnemySpawnPoints;
	std::vector<Vehicle*>				mActiveEnemies;


	sf::Clock deltaClock;
	sf::Time timeSinceLastFrame;

	SceneNode*							suddenDeathTextNode;
	BloomEffect							mBloomEffect;

	bool								mNetworkedWorld;
	NetworkNode*						mNetworkNode;
	SpriteNode*							mFinishSprite;
};