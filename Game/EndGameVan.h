#pragma once
#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"

#include <SFML/Graphics/Sprite.hpp>

class EndGameVan : public Entity
{
public:

	EndGameVan(const TextureHolder& textures);

	virtual unsigned int	getCategory() const;
	virtual sf::FloatRect	getBoundingRect() const;
	sf::Sprite				mSprite;


protected:
	virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
};