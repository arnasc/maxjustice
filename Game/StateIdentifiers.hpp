#pragma once
namespace States
{
	enum ID
	{
		None,
		Title,
		Menu,
		Game,
		Loading,
		Pause,
		NetworkPause,
		Settings,
		GameOver,
		MissionSuccess,
		Instructions,
		HostGame,
		JoinGame,
		RobbersWin,
		JusticeWin
	};
}
