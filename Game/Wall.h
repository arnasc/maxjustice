#pragma once
#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"

#include <SFML/Graphics/Sprite.hpp>

class Wall : public Entity
{
public:
	enum Type
	{
		Straight,
		TypeCount
	};

public:

	Wall(Type type, const TextureHolder& textures);

	virtual unsigned int	getCategory() const;
	virtual sf::FloatRect	getBoundingRect() const;
	sf::Sprite				mSprite;


	protected:
	virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	Type 					mType;
};