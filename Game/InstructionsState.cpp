#include "InstructionsState.h"
#include "Utility.hpp"
#include "ResourceHolder.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

InstructionsState::InstructionsState(StateStack& stack, Context context)
	: State(stack, context)
	, mGUIContainer()
{
	mBackgroundSprite.setTexture(context.textures->get(Textures::InstructionsScreen));

	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setPosition(300.f, 900.f);
	backButton->setText("Back");
	backButton->changeTexture(GUI::Button::BackNormal);
	backButton->setCallback(std::bind(&InstructionsState::requestStackPop, this));

	mGUIContainer.pack(backButton);
}

void InstructionsState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	window.draw(mBackgroundSprite);
	window.draw(mGUIContainer);
}

bool InstructionsState::update(sf::Time)
{
	return true;
}

bool InstructionsState::handleEvent(const sf::Event& event)
{
	bool isKeyBinding = false;
	/*
	// Iterate through all key binding buttons to see if they are being pressed, waiting for the user to enter a key
	for (std::size_t i = 0; i < 2*PlayerAction::Count; ++i)
	{
	if (mBindingButtons[i]->isActive())
	{
	isKeyBinding = true;
	if (event.type == sf::Event::KeyReleased)
	{
	// Player 1
	if (i < PlayerAction::Count)
	getContext().keys1->assignKey(static_cast<PlayerAction::Type>(i), event.key.code);

	// Player 2
	else
	getContext().keys2->assignKey(static_cast<PlayerAction::Type>(i - PlayerAction::Count), event.key.code);

	mBindingButtons[i]->deactivate();
	}
	break;
	}
	}

	// If pressed button changed key bindings, update labels; otherwise consider other buttons in container
	if (isKeyBinding)
	updateLabels();
	else*/
	mGUIContainer.handleEvent(event);

	return false;
}